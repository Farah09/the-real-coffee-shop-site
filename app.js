require('dotenv').config();

const express = require('express'),
      app = express(),
      bodyParser = require('body-parser'),
      session = require('express-session'),
      passportLocalMongoose = require('passport-local-mongoose'),
      router = express.Router(),
      path = require('path'),
      favicon = require('serve-favicon'),
      logger = require('morgan'),
      cookieParser = require('cookie-parser'),
      passport = require('passport');
      LocalStrategy = require('passport-local').Strategy,
      flash = require('connect-flash'),
      // configuration de la base de donnée
      mongoose = require('mongoose'),
      configDB = require('./config/database.js');
      mongoose.connect(configDB.url, {useNewUrlParser: true , useUnifiedTopology: true} );

const routes = require('./routes/index'),
      users = require('./routes/users');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public'))); // Pour importer des fichier css
app.use(express.static('files')); // pour pouvoir importer les image du fichier public

app.use(session({ secret: 'shhsecret', resave: true, saveUninitialized: true }));
app.use(passport.initialize());
app.use(passport.session());
app.use(flash());

require('./config/passport')(passport);

app.use('/', routes);
app.use('/users', users);

app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err,
    });
  });
}

app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {},
  });
});



app.listen(8050, function () {
    console.log('APP écoute serveur 8050');
})

module.exports = app;
