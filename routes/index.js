var express = require('express');
var passport = require('passport');
var router = express.Router();

router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/auth', function(req, res, next) {
  res.render('auth.ejs', { message: req.flash('loginMessage') });
});

router.get('/admin', isLoggedIn, function(req, res) {
  res.render('admin.ejs', { user: req.user });
});

router.get('/laCarte', function(req, res) {
    res.render('laCarte.ejs', {});
});

router.get('/aProposDeNous', function(req, res) {
    res.render('aProposDeNous.ejs', {});
});

router.get('/nousContacter', function(req, res) {
    res.render('nousContacter.ejs', {});
});

router.get('/logout', function(req, res) {
  req.logout();
  res.redirect('/');
});

router.post('/signup', passport.authenticate('local-signup', {
  successRedirect: '/admin',
  failureRedirect: '/signup',
  failureFlash: true,
}));

router.post('/login', passport.authenticate('local-login', {
  successRedirect: '/admin',
  failureRedirect: '/login',
  failureFlash: true,
}));

router.get('/auth/facebook', passport.authenticate('facebook', { scope: 'email' }));

router.get('/auth/facebook/callback', passport.authenticate('facebook', {
  successRedirect: '/admin',
  failureRedirect: '/',
}));

router.get('/auth/twitter', passport.authenticate('twitter'));

router.get('/auth/twitter/callback', passport.authenticate('twitter', {
  successRedirect: '/admin',
  failureRedirect: '/',
}));

router.get('/auth/google', passport.authenticate('google', { scope: ['admin', 'email'] }));

router.get('/auth/google/callback', passport.authenticate('google', {
  successRedirect: '/admin',
  failureRedirect: '/',
}));

module.exports = router;

function isLoggedIn(req, res, next) {
  if (req.isAuthenticated())
      return next();
  res.redirect('/');
}
